#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "SRVideoBottomBar.h"
#import "SRVideoLayerView.h"
#import "SRVideoOperationTip.h"
#import "SRVideoPlayer.h"
#import "SRVideoTopBar.h"

FOUNDATION_EXPORT double SRVideoPlayerVersionNumber;
FOUNDATION_EXPORT const unsigned char SRVideoPlayerVersionString[];

