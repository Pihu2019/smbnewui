
#import <UIKit/UIKit.h>
#import "SSSearchBar.h"
#import <SafariServices/SafariServices.h>


@interface AudioListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,NSURLSessionDelegate,UITextViewDelegate,UISearchBarDelegate,UISearchControllerDelegate,SSSearchBarDelegate,SFSafariViewControllerDelegate>
{
    dispatch_queue_t queue;
    NSInteger indexBtn;
    NSData *data;
    NSString *filePath;
    
}
@property (weak, nonatomic) IBOutlet UITableView *tableview;

@property (nonatomic,strong) NSMutableArray *audioListArr,*filteredArray;

@property(nonatomic,retain)NSString *status,*indexp,*message,*details,*currentPageNo,*totalCount,*totalPageSize,*fileIdStr,*filenameStr,*bhajanTitle,*audioPath;

@property(nonatomic,retain)UIImage *audioImg;
@property (nonatomic,strong) NSMutableArray *imgarray;

-(void)dataAudioParsing;

@property (weak, nonatomic) IBOutlet SSSearchBar *searchBar;



@end
