//
//  RegisterViewController.m
//  SanjayMittalBhajan
//
//  Created by Punit on 01/08/18.
//  Copyright © 2018 Eshiksa. All rights reserved.
//

#import "RegisterViewController.h"
#import "Base.h"
#import "Constant.h"
#import "Login.h"
#import "Role.h"
@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
   // [_username becomeFirstResponder];

    _username.delegate=self;
    _address.delegate=self;
    _email.delegate=self;
    _mobilenum.delegate=self;
    _password.delegate=self;
    _confirmPassword.delegate=self;
    
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    [self.navigationController.navigationBar setTitleTextAttributes:@{
                                                                      NSForegroundColorAttributeName : [UIColor clearColor]
                                                                      }];
    
    [_usernameView.layer setCornerRadius:20.0f];
    [_usernameView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [_usernameView.layer setBorderWidth:1.5f];
    
    [_passView.layer setCornerRadius:20.0f];
    [_passView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [_passView.layer setBorderWidth:1.5f];
    
    [_confirmPassView.layer setCornerRadius:20.0f];
    [_confirmPassView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [_confirmPassView.layer setBorderWidth:1.5f];
    
    [_emailView.layer setCornerRadius:20.0f];
    [_emailView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [_emailView.layer setBorderWidth:1.5f];
    
    [_addressView.layer setCornerRadius:20.0f];
    [_addressView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [_addressView.layer setBorderWidth:1.5f];
    
    [_contactView.layer setCornerRadius:20.0f];
    [_contactView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [_contactView.layer setBorderWidth:1.5f];
    
    
    UIColor *color = [UIColor grayColor];
    _username.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Username" attributes:@{NSForegroundColorAttributeName: color}];
    _address.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Address" attributes:@{NSForegroundColorAttributeName: color}];
    _email.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter email" attributes:@{NSForegroundColorAttributeName: color}];
    _mobilenum.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Contact No." attributes:@{NSForegroundColorAttributeName: color}];
    _password.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];
    _confirmPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Confirm password" attributes:@{NSForegroundColorAttributeName: color}];
    
    CAGradientLayer *gradient1 = [CAGradientLayer layer];
    gradient1.frame = _createAccBtn.bounds;
    gradient1.colors = @[(id)[UIColor colorWithRed:(240.0/225.0) green:(93.0/225.0) blue:(0.0/255.0)alpha:1.0].CGColor,(id)[UIColor colorWithRed:(241.0/225.0) green:(139.0/225.0) blue:(0.0/255.0)alpha:1.0].CGColor];
    
    [_createAccBtn.layer insertSublayer:gradient1 atIndex:0];
    _createAccBtn.layer.cornerRadius = 20; // this value vary as per your desire
    _createAccBtn.clipsToBounds = YES;

    
    
    
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.username resignFirstResponder];
    [self.address resignFirstResponder];
    [self.email resignFirstResponder];
    [self.mobilenum resignFirstResponder];
    [self.password resignFirstResponder];
    [self.confirmPassword resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField==self.username)
    {
        [textField resignFirstResponder];
        [self.address becomeFirstResponder];
    }
     else if(textField==self.address)
    {
        [textField resignFirstResponder];
        [self.email becomeFirstResponder];
    }
     else if(textField==self.email)
    {
        [textField resignFirstResponder];
        [self.mobilenum becomeFirstResponder];
    }
    else if(textField==self.mobilenum)
    {
        [textField resignFirstResponder];
        [self.password becomeFirstResponder];
    }
    else if(textField==self.password)
    {
        [textField resignFirstResponder];
        [self.confirmPassword becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    return  YES;
}
- (IBAction)createAccountBtnClicked:(id)sender {
    
    [self requestRegistrationdata];
}

-(void)requestRegistrationdata{
    
    NSDictionary *data = @{ @"username":self.username.text ,
     @"address":self.address.text ,
     @"email":self.email.text ,
     @"contactNumber":self.mobilenum.text ,
     @"password": self.password.text
     };
     
     NSError *error;
     NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data options:kNilOptions error:&error];
     
     NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[mainUrl stringByAppendingString:registration]]];
     
     NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url cachePolicy:nil timeoutInterval:60];
     [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
     [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
     [req setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[jsonData length]] forHTTPHeaderField:@"Content-Length"];
     [req setHTTPMethod:@"POST"];
     [req setHTTPBody:jsonData];
     
     NSString *retStr = [[NSString alloc] initWithData:[NSURLConnection sendSynchronousRequest:req returningResponse:nil error:nil] encoding:NSUTF8StringEncoding];
     
     NSLog(@"Return resistration String%@",retStr);
    
    
     
    NSDictionary *maindic=[NSJSONSerialization JSONObjectWithData:[NSURLConnection sendSynchronousRequest:req returningResponse:nil error:nil] options:NSJSONReadingMutableContainers error:nil];
    NSLog(@"response data:%@",maindic);
    
            UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Success" message:@"User Registered successfully" preferredStyle:UIAlertControllerStyleAlert];
    
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
    
                                     [alertView dismissViewControllerAnimated:YES completion:nil];
                                     [self viewWillAppear:YES];
                                     self.username.text=@"";
                                     self.address.text=@"";
                                     self.email.text=@"";
                                     self.mobilenum.text=@"";
                                     self.password.text=@"";
                                     self.confirmPassword.text=@"";
    
    
                                 }];
    
            [alertView addAction:ok];
           [self presentViewController:alertView animated:YES completion:nil];
    
    self.status=[maindic objectForKey:@"status"];
    self.message=[maindic objectForKey:@"message"];
    
    NSArray *detailArr=[maindic objectForKey:@"details"];
    
    NSLog(@"status==%@& message=%@ details==%@",self.status,self.message,detailArr);
    
    [[NSOperationQueue mainQueue]addOperationWithBlock:^{
        
    }];
    if ([self.status isEqual:@"1"]) {
        
//        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Success" message:@"Registered successfully" preferredStyle:UIAlertControllerStyleAlert];
//
//        UIAlertAction* ok = [UIAlertAction
//                             actionWithTitle:@"OK"
//                             style:UIAlertActionStyleDefault
//                             handler:^(UIAlertAction * action)
//                             {
//
//                                 [alertView dismissViewControllerAnimated:YES completion:nil];
//                                 //[self performSegueWithIdentifier:@"Home" sender:self];
//
//
//                             }];
//
//        [alertView addAction:ok];
//        [[NSUserDefaults standardUserDefaults] setObject:self.email.text forKey:@"email"];
//        [[NSUserDefaults standardUserDefaults] synchronize];
        
   //     [self presentViewController:alertView animated:YES completion:nil];
        

    }
    else{
//                UIAlertController *alertView = [UIAlertController alertControllerWithTitle:nil message:@"Entered credential is incorrect" preferredStyle:UIAlertControllerStyleAlert];
//
//                UIAlertAction* ok = [UIAlertAction
//                                     actionWithTitle:@"OK"
//                                     style:UIAlertActionStyleDefault
//                                     handler:^(UIAlertAction * action)
//                                     {
//                                         [alertView dismissViewControllerAnimated:YES completion:nil];
//
//                                     }];
//
//                [alertView addAction:ok];
//                [self presentViewController:alertView animated:YES completion:nil];
    }

}


@end
