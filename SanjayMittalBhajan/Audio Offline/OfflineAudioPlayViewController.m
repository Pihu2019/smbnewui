
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import "OfflineAudioPlayViewController.h"
#import "MBProgressHUD.h"
#import <QuartzCore/QuartzCore.h>
#import "DGActivityIndicatorView.h"
#import "OfflineViewController.h"
#import "MyAudioPlayer.h"
@interface OfflineAudioPlayViewController ()
{
    NSString *finalUrl,*str;
    BOOL isSelected;
    NSURL *url;
    NSUInteger selectedPosition,previousPositionNew;
    AVAudioPlayer *player;
    DGActivityIndicatorView *activityIndicatorView;
    MyAudioPlayer * myAudioPlayer;
    NSString * documentsDirectory;
}

@end

@implementation OfflineAudioPlayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:NO];
    hud.mode = MBProgressHUDAnimationFade;
    hud.labelText = @"Buffering...";
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeLineScale tintColor:[UIColor whiteColor]];
    CGFloat width = self.view.bounds.size.width / 6.0f;
    CGFloat height = self.view.bounds.size.height / 6.0f;
    activityIndicatorView.frame = CGRectMake(20,40,width,height);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    documentsDirectory = [paths objectAtIndex:0];
    
    myAudioPlayer = [MyAudioPlayer sharedInstance];
    player = myAudioPlayer.mAudioPlayer;
    
    [self playSongFromDataAt:self.selectedIndex];
    [self.view addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    
    [MBProgressHUD hideHUDForView:self.view animated:NO];
    
    
}




-(void)playSongFromDataAt:(int) index{
    
    dispatch_async( dispatch_get_main_queue(),
                   ^{
                       if([self->player isPlaying]){
                           [self->player stop];
                           self.progressSlider.value=0.0;
                           [self->activityIndicatorView stopAnimating];
                           //mPlayer = nil;
                       }
                       NSString * filePath = [self->documentsDirectory stringByAppendingPathComponent:[self->_audiopathArray objectAtIndex:(NSUInteger)self.selectedIndex]];
                       
                       NSURL * localURL = [NSURL fileURLWithPath:filePath];
                       
                       NSString *fileNameStr=[self->_audiopathArray objectAtIndex:(NSUInteger)self.selectedIndex];
                       NSRange range = [fileNameStr rangeOfString:@"."];
                       if (range.location != NSNotFound) {
                           self.audioTitle.text  = [fileNameStr substringToIndex:range.location];
                           NSLog(@"audioTitle.text %@", self.audioTitle.text);
                       } else {
                           NSLog(@". is not found");
                       }
                       [self playSongWithURL:localURL];
                       
                       NSLog(@"local urllll**%@",localURL);
                       self.progressSlider.maximumValue=self->player.duration;
                       
                       self.sliderTimer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateSlider) userInfo:nil repeats:YES];
                       
                       [self.progressSlider addTarget:self action:@selector(progressSliderChanged:) forControlEvents:UIControlEventValueChanged];
                   });
    self.progressSlider.value=0.0;
    self.durationTimeLabel.text=[self stringFromInterval:player.duration];
    
    if (player.duration <= 3600) {
        self.currentTimeLabel.text=[NSString stringWithFormat:@"00:00"];
    }
    else{
        self.currentTimeLabel.text=[NSString stringWithFormat:@"0:00:00"];
    }
    [self.currentTimeLabel sizeToFit];
}
-(void)playSongWithURL:(NSURL *)audioUrl{
    
    
    NSError*error;
    
    error = [myAudioPlayer playSongWithFileURL:audioUrl];
    player = myAudioPlayer.mAudioPlayer;
    player.delegate=self;
    
    
    if (error) {
        NSLog(@"FAILURE:%@",error.localizedDescription);
    }
    else{
        self.progressSlider.value=0.0;
        self.durationTimeLabel.text=[self stringFromInterval:player.duration];
        
        if (player.duration <= 3600) {
            self.currentTimeLabel.text=[NSString stringWithFormat:@"00:00"];
        }
        else{
            self.currentTimeLabel.text=[NSString stringWithFormat:@"0:00:00"];
        }
        [self.currentTimeLabel sizeToFit];
        
        // [player prepareToPlay];
        NSLog(@"****Player start*****");
        [player play];
        
        
        [self.view addSubview:activityIndicatorView];
        [activityIndicatorView startAnimating];
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        
    }
    
}
-(NSURL*)audioMakeURL:(NSString *)audioPath{
    
    NSURL * localURL = [NSURL fileURLWithPath:_audiopathStr];
    NSLog(@"****localURL :%@",localURL);
    
    url=localURL;
    NSLog(@"url***%@",url);
    
    return localURL;
    
    
}
-(NSString *)stringFromInterval:(NSTimeInterval)interval{
    
    NSInteger ti=(NSInteger)interval;
    int seconds=ti % 60;
    int minutes=(ti/60)%60;
    long int hourss=(ti / 3600);
    if (ti <= 3600) {
        return [NSString stringWithFormat:@"%02d:%02d",minutes,seconds];
    }
    return [NSString stringWithFormat:@"%ld:%02d:%02d",hourss,minutes,seconds];
    
    return 0;
}
- (IBAction)playPauseAudio:(id)sender {
    [self playPauseMethod];
    NSLog(@"PLAY PAUSE BTN CLICKED..");
}
-(void)playPauseMethod{
    if (!player.playing) {
        self.progressSlider.maximumValue=player.duration;
        
        self.sliderTimer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateSlider) userInfo:nil repeats:YES];
        
        [self.progressSlider addTarget:self action:@selector(progressSliderChanged:) forControlEvents:UIControlEventValueChanged];
        
        [player play];
        [activityIndicatorView startAnimating];
        [self.playPauseBtn setImage:[UIImage imageNamed:@"pausebutton36.png"] forState:UIControlStateNormal];
        
    }else if (player.playing){
        [player pause];
        [activityIndicatorView stopAnimating];
        [self.playPauseBtn setImage:[UIImage imageNamed:@"playbutton36.png"] forState:UIControlStateNormal];
    }
    
}
-(void)updateSlider{
    self.progressSlider.value=player.currentTime;
    self.currentTimeLabel.text=[self stringFromInterval:player.currentTime];
    
}
- (IBAction)progressSliderChanged:(id)sender {
    [player stop];
    [player setCurrentTime:self.progressSlider.value];
    [player prepareToPlay];
    [player play];
}
- (IBAction)nextBtnClicked:(id)sender {
    NSLog(@"next button clicked");
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:NO];
    hud.mode = MBProgressHUDModeAnnularDeterminate;
    hud.labelText = @"Buffering...";
    CFTimeInterval startTime = CACurrentMediaTime();
    _nextIndex=_selectedIndex+1;
    if(_nextIndex < _audiopathArray.count)
    {
        _previousIndex = _selectedIndex;
        _selectedIndex =_nextIndex;
        [self playSongFromDataAt:self.selectedIndex];
        _nextIndex++;
    }
    [MBProgressHUD hideHUDForView:self.view animated:NO];
    CFTimeInterval elapsedTime = CACurrentMediaTime() - startTime;
    NSLog(@"****next button clicked elapsedTime:%f",elapsedTime);//14.909196
}

- (IBAction)previousBtnClicked:(id)sender {
    NSLog(@"previous  button clicked");
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:NO];
    hud.mode = MBProgressHUDModeAnnularDeterminate;
    hud.labelText = @"Buffering...";
    
    CFTimeInterval startTime = CACurrentMediaTime();
    _previousIndex=_selectedIndex-1;
    if(_previousIndex >= 0)
    {
        _nextIndex = _selectedIndex;
        _selectedIndex= _previousIndex;
        [self playSongFromDataAt:self.selectedIndex];
        _previousIndex--;
    }
    [MBProgressHUD hideHUDForView:self.view animated:NO];
    CFTimeInterval elapsedTime = CACurrentMediaTime() - startTime;
    NSLog(@"****previous button clicked elapsedTime:%f",elapsedTime);//11.423854
}



@end

