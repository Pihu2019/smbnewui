
#import "VideoOfflineViewController.h"
#import "VideoOfflineTableViewCell.h"
#import "VideoPlayOfflineVC.h"
#import "Base.h"

@interface VideoOfflineViewController ()
{
    int indxp;
    NSArray *filePathsArray;
    BOOL isDataLoading;
    NSString *path;
}
@end

@implementation VideoOfflineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _tableview.delegate=self;
    _tableview.dataSource=self;
    [self.tableview setSeparatorColor:[UIColor clearColor]];
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    // filePathsArray = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory  error:nil];
    filePathsArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:documentsDirectory  error:nil];
    
    _arrMp4Files=[[NSMutableArray alloc]init];
    
    for (NSString *str in filePathsArray) {
        NSString *strFileName=[str.lastPathComponent lowercaseString];
        if([strFileName.pathExtension isEqualToString:@"mp4"])
        {
            NSLog(@"stringggg   %@",str);
            [_arrMp4Files addObject:str];
            NSLog(@"arrMp3Files  %@",_arrMp4Files);
        }
    }
    
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   
    return  _arrMp4Files.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    VideoOfflineTableViewCell *cell = [_tableview dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    filePathsArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:documentsDirectory  error:nil];
    path = [documentsDirectory stringByAppendingPathComponent:[_arrMp4Files objectAtIndex:indexPath.row]];
    
    NSLog(@"path===%@", path);
    cell.videofilePath.text=path;
    NSURL *url = CFBridgingRelease(CFURLCreateWithFileSystemPath(kCFAllocatorDefault, (CFStringRef)path, kCFURLWindowsPathStyle, false));
    NSString *fileName = url.lastPathComponent;
    NSLog(@"fileName===%@",fileName);
    
    NSRange range = [fileName rangeOfString:@"."];
    if (range.location != NSNotFound) {
        cell.videoTitle.text = [fileName substringToIndex:range.location];
        NSLog(@" cell.audioTitle.text %@", cell.videoTitle.text);
    } else {
        NSLog(@". is not found");
    }
    
    return cell;
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 106;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    VideoOfflineTableViewCell *cell=[_tableview cellForRowAtIndexPath:indexPath];
    
    _videoTitle=cell.videoTitle.text;
    path=cell.videofilePath.text;
    
    indxp=indexPath.row;
    
    NSLog(@"audio indexpath==%ld audiopath=%@  path=%@",(long)indexPath.row,_videoTitle,path);
    
    
    
    dispatch_async( dispatch_get_main_queue(),
                   ^{
                       [self performSegueWithIdentifier:@"offlineAudioPlay"
                                                 sender:[self.tableview cellForRowAtIndexPath:indexPath]];
                   });
    
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([[segue identifier] isEqualToString:@"offlineAudioPlay"])
    {
        
        VideoPlayOfflineVC *kvc = [segue destinationViewController];
        
        kvc.videoPath=path;
        kvc.videolabelStr=_videoTitle;
        kvc.selectedIndex=indxp;
        
        NSLog(@"_audioPath==%@ & _bhajanTitle =%@ selected index =%d ",path,_videoTitle,indxp);
        
    }
}


@end
