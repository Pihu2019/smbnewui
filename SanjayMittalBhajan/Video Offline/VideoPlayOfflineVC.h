

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface VideoPlayOfflineVC : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *videopath;
@property (weak, nonatomic) IBOutlet UILabel *videolbl;
@property (weak, nonatomic) IBOutlet UIView *videoView;
@property(nonatomic,retain)NSString *indxp,*videoPath,*videolabelStr;
@property(nonatomic,readwrite) int selectedIndex;

@end

NS_ASSUME_NONNULL_END
