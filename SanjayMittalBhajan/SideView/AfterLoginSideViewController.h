//
//  AfterLoginSideViewController.h
//  SanjayMittalBhajan
//
//  Created by Punit on 01/11/18.
//  Copyright © 2018 Eshiksa. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AfterLoginSideViewController : UIViewController<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imageViewSideBar;
@property (weak, nonatomic) IBOutlet UIView *sideView;
@property (weak, nonatomic) IBOutlet UILabel *username;



@end

NS_ASSUME_NONNULL_END
