
//  AfterLoginSideViewController.m

#import "AfterLoginSideViewController.h"

@interface AfterLoginSideViewController ()

@end

@implementation AfterLoginSideViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.imageViewSideBar.layer.cornerRadius = self.imageViewSideBar.frame.size.width / 2;
    self.imageViewSideBar.clipsToBounds = YES;
    
    self.navigationItem.backBarButtonItem.title=@"Back";
    UIBarButtonItem *newBackButton =
    [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    [[self navigationItem] setBackBarButtonItem:newBackButton];
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_sideView.bounds
                                                   byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight)
                                                         cornerRadii:CGSizeMake(0.0, 0.0)];
    
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = _sideView.bounds;
    maskLayer.path = maskPath.CGPath;
    _sideView.layer.mask = maskLayer;
    
    
    NSString *username = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"username"];
    NSLog(@"username==%@",username);
    self.username.text=username;
    
}
- (IBAction)shareButton:(UIButton *)sender
{
    NSString *textToShare = @"Sanjay Mittal Bhajans Mobile APP is a Sewa towards Shyam Baba with a mission to reach maximum Shyam Premis";
    NSURL *myWebsite = [NSURL URLWithString:@"https://play.google.com/store/apps/details?id=com.getEKart.sanjaymittalbhajans"];
    
    NSArray *objectsToShare = @[textToShare, myWebsite];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    //if iPhone
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                       UIActivityTypePrint,
                                       UIActivityTypeAssignToContact,
                                       UIActivityTypeSaveToCameraRoll,
                                       UIActivityTypeAddToReadingList,
                                       UIActivityTypePostToFlickr,
                                       UIActivityTypePostToVimeo,
                                       UIActivityTypePostToTwitter,
                                       UIActivityTypePostToFacebook,
                                       UIActivityTypePostToWeibo,
                                       UIActivityTypeMessage,
                                       UIActivityTypeMail,
                                       UIActivityTypeCopyToPasteboard,
                                       UIActivityTypePostToTencentWeibo];
        
        activityVC.excludedActivityTypes = excludeActivities;
        
        [self presentViewController:activityVC animated:YES completion:nil];
        
    }
    //if iPad
    else {
        // Change Rect to position Popover
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:activityVC];
        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}
- (IBAction)logoutBtnClicked:(id)sender {
    
    
    [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"username"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
@end
