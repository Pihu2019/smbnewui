#import "VideoPlayViewController.h"
#import "SRVideoPlayer.h"
#import "Base.h"
#import "MyAudioPlayer.h"

@interface VideoPlayViewController ()
{
    NSURL *url;
    AVAudioPlayer * mPlayer;
    MyAudioPlayer * myAudioPlayer;

}
@property (nonatomic, strong) SRVideoPlayer *videoPlayer;
@end

@implementation VideoPlayViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blackColor];
    [self showVideoPlayer];
    
  
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [_videoPlayer destroyPlayer];
}

- (void)showVideoPlayer {
    UIView *playerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.width)];
    playerView.center = self.view.center;
    [self.view addSubview:playerView];
    
    self.videolbl.text=_videolabel;
    self.videopath.text=_videoPath;
    NSLog(@"video path=%@ video lbl=%@",_videoPath,_videolabel);
    
    NSString *mainstr=[NSString stringWithFormat:@"%@",[videoUrl stringByAppendingString:_videoPath]];
    
    NSLog(@"*******mainstr=%@",mainstr);
    
    url=[NSURL URLWithString:mainstr];
    
    _videoPlayer = [SRVideoPlayer playerWithVideoURL:url playerView:playerView playerSuperView:playerView.superview];
    
    //_videoPlayer.videoName = @"Here Is The Video Name";
    //    _videoPlayer.playerEndAction = SRVideoPlayerEndActionLoop;
    [_videoPlayer play];
    
    myAudioPlayer = [MyAudioPlayer sharedInstance];
    mPlayer = myAudioPlayer.mAudioPlayer;
    mPlayer.delegate=self;
    [mPlayer stop];

}

@end
