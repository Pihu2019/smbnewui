

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>
#import <AudioToolbox/AudioToolbox.h>
#import "AudioInfo.h"
NS_ASSUME_NONNULL_BEGIN

@interface VideoPlayViewController : UIViewController<AVAudioPlayerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *videopath;
@property (weak, nonatomic) IBOutlet UILabel *videolbl;
@property (weak, nonatomic) IBOutlet UIView *videoView;
@property(nonatomic,retain)NSString *indxp,*videoPath,*videolabel;
@property(strong,nonatomic)AVAudioPlayer *audioPlayer;
@end

NS_ASSUME_NONNULL_END
