

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import "SSSearchBar.h"
#import <SafariServices/SafariServices.h>

@interface VideoViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,NSURLSessionDelegate,UITextViewDelegate,UISearchBarDelegate,UISearchControllerDelegate,SSSearchBarDelegate>
{
    NSInteger indexBtn;
    NSData *data;
    NSString *filePath;
}
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (nonatomic,strong) NSMutableArray *videoListArr,*filteredArray;

@property(nonatomic,retain)NSString *indxp,*status,*message,*details,*currentPageNo,*totalCount,*totalPageSize,*videoPath,*videolbl,*indexp,*filename;


@property (weak, nonatomic) IBOutlet UIView *videoView;
@property (weak, nonatomic) IBOutlet SSSearchBar *searchBar;
@end
