
//https://www.techotopia.com/index.php/Video_Playback_from_within_an_iOS_7_Application

#import "VideoViewController.h"
#import "VideoTableViewCell.h"
#import "Base.h"
#import "VideoPlayViewController.h"
#import "Reachability.h"
#import "MBProgressHUD.h"
#import "DGActivityIndicatorView.h"
#import "UIScrollView+InfiniteScroll.h"
#import "CustomInfiniteIndicator.h"
#import "UIApplication+NetworkIndicator.h"


static NSString *const kAPIEndpointURL = @"http://35.200.153.165:8080/Sanjay_Mittal_Bhajans/apis/video/video-list?pageNo=%d";
static NSString *const kJSONResultsKey = @"hits";
static NSString *const kJSONNumPagesKey = @"nbPages";

@interface VideoViewController ()
{
    BOOL isFiltered;
//    int startPage;
//    int lastPage ;
    NSString*videoPathStr;
    DGActivityIndicatorView *activityIndicatorView;
}
@property (nonatomic) NSInteger currentPage;
@property (nonatomic) NSInteger numPages;
@end

@implementation VideoViewController
@synthesize searchBar;
- (void)viewDidLoad {
    [super viewDidLoad];
//    startPage = 1 ;
//    lastPage = 1 ;
    [self.tableview setSeparatorColor:[UIColor clearColor]];
    self.searchBar.cancelButtonHidden = NO;
    self.searchBar.placeholder = NSLocalizedString(@"Search text here!", nil);
    self.searchBar.delegate = self;
    //[self.searchBar becomeFirstResponder];
    
    
#if USE_AUTOSIZING_CELLS
    // enable auto-sizing cells on iOS 8
    if([self.tableView respondsToSelector:@selector(layoutMargins)]) {
        self.tableView.estimatedRowHeight = 88.0;
        self.tableView.rowHeight = UITableViewAutomaticDimension;
    }
#endif
    //    startPage = 1 ;
    //    lastPage = 1 ;
    self.currentPage = 0;
    self.numPages = 0;
    
    __weak typeof(self) weakSelf = self;
    
    // Create custom indicator
    CGRect indicatorRect;
    
#if TARGET_OS_TV
    indicatorRect = CGRectMake(0, 0, 64, 64);
#else
    indicatorRect = CGRectMake(0, 0, 24, 24);
#endif
    
    CustomInfiniteIndicator *indicator = [[CustomInfiniteIndicator alloc] initWithFrame:indicatorRect];
    
    // Set custom indicator
    self.tableview.infiniteScrollIndicatorView = indicator;
    
    // Set custom indicator margin
    self.tableview.infiniteScrollIndicatorMargin = 40;
    
    // Set custom trigger offset
    self.tableview.infiniteScrollTriggerOffset = 500;
    
    // Add infinite scroll handler
    [self.tableview addInfiniteScrollWithHandler:^(UITableView *tableView) {
        [weakSelf fetchData:^{
            // Finish infinite scroll animations
            [self.tableview finishInfiniteScroll];
        }];
    }];
    
    // Load initial data
    [self.tableview beginInfiniteScroll:YES];
    
    
    searchBar.delegate=(id)self;
    _tableview.delegate=self;
    _tableview.dataSource=self;
    _videoListArr=[[NSMutableArray alloc]init];
    
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"Alert!" message:@"Please check internet connection" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *action=[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                               
                               {
                               }];
        
        [alert addAction:action];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.tableview animated:NO];
        hud.mode = MBProgressHUDAnimationFade;
        hud.labelText = @"Loading...";
        
        activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeLineScale tintColor:[UIColor whiteColor]];
        CGFloat width = self.view.bounds.size.width / 6.0f;
        CGFloat height = self.view.bounds.size.height / 6.0f;
        activityIndicatorView.frame = CGRectMake(20,40,width,height);
        
         [self dataVideoParsing];
        
        
    }
  
}
#pragma mark - Actions

- (IBAction)handleRefresh {
      [self.tableview beginInfiniteScroll:YES];
}

#pragma mark - SSSearchBarDelegate

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    [searchBar becomeFirstResponder];
}

- (void)searchBarCancelButtonClicked:(SSSearchBar *)searchBar {
    self.searchBar.text = @"";
    
}

-(void)viewDidUnload{
    [self setSearchBar:nil];
    [super viewDidUnload];
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSLog(@"****searchText:%@",searchText);
    
    if(searchText.length == 0)
    {
        isFiltered = FALSE;
    }
    else
    {
        isFiltered = true;
         _filteredArray=[[NSMutableArray alloc]init];
        for (NSDictionary *temp in _videoListArr)
        {
            NSRange nameRange = [[[temp objectForKey:@"tags"]description] rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if(nameRange.location != NSNotFound)
            {
                [_filteredArray addObject:temp];
            }
        }
    }
    
    [self.tableview reloadData];
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.tableview resignFirstResponder];
}
-(void)dataVideoParsing{
    if (_currentPage<=_numPages) {
        NSLog(@"start page=== %ld",_currentPage) ;
        
  
    NSString *username = @"priya0102";
    NSString *password = @"priya12345";
    
    NSString *unpw = [NSString stringWithFormat:@"%@:%@",username,password];
    NSData *updata = [unpw dataUsingEncoding:NSASCIIStringEncoding];
    
    NSString *base64str = [NSString stringWithFormat:@"Basic %@", [updata base64Encoding]];
    NSDictionary *headers = @{ @"content-type": @"json/application",
                               @"authorization": base64str };

        NSString *mainstr=[NSString stringWithFormat:@"http://35.200.153.165:8080/Sanjay_Mittal_Bhajans/apis/video/video-list?pageNo=%ld",(long)_currentPage];
        
            NSLog(@"*****url string==%@",mainstr);
        
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:mainstr] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
         dispatch_async(dispatch_get_main_queue(), ^{
        if (error) {
            NSLog(@"%@", error);
        }
        else {
            
            NSLog(@"Success: %@", data);
            
            NSError *err;
            
            NSArray *jsonArray  = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
            NSLog(@"JSON DATA%@",jsonArray);
            
            NSDictionary *maindic=[NSJSONSerialization JSONObjectWithData:[NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil] options:NSJSONReadingMutableContainers error:nil];
            NSLog(@"response data:%@",maindic);
            
            self.status=[maindic objectForKey:@"status"];
            self.message=[maindic objectForKey:@"message"];
            
            NSArray *detailArr=[maindic objectForKey:@"details"];
            NSDictionary *bodyDic=[maindic objectForKey:@"body"];
            
            NSLog(@"status==%@& message=%@ details==%@  bodyDic==%@",self.status,self.message,detailArr,bodyDic);
            
            self.currentPageNo=[bodyDic objectForKey:@"currentPageNo"];
            self.totalCount=[bodyDic objectForKey:@"totalCount"];
//            self.totalPageSize=[bodyDic objectForKey:@"totalPageSize"];
//            NSLog(@"status==%@",self.totalCount);
             self->_numPages = [bodyDic[@"totalPageSize"] intValue];
            NSLog(@"totalcount==%@, totalpagesize==%ld",self.totalCount, self->_numPages);
            NSArray *bodyArr=[bodyDic objectForKey:@"body"];
            
            
            if(bodyArr.count==0)
            {
                UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Sorry!" message:@"Currently there is no video data." preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [alertView dismissViewControllerAnimated:YES completion:nil];
                                         
                                     }];
                
                
                [alertView addAction:ok];
                
                [self presentViewController:alertView animated:YES completion:nil];
                
            }
            else {
                
                for(NSDictionary *temp in bodyArr)
                {
                    NSString *str1=[[temp objectForKey:@"thumbnailsPath"]description];
                    NSString *str2=[[temp objectForKey:@"videoPath"]description];
                    NSString *str3=[[temp objectForKey:@"fileName"]description];
                    NSString *str4=[[temp objectForKey:@"videoSize"]description];
                    NSString *str5=[[temp objectForKey:@"tags"]description];
                    NSString *str6=[[temp objectForKey:@"bhajanTitle"]description];
                     NSString *str7=[[temp objectForKey:@"fileId"]description];
                    
                    NSLog(@"date=%@  day=%@ venue=%@ organisation=%@  contactPerson=%@  contactNumber=%@fileId=%@",str1,str2,str3,str4,str5,str6,str7);
                    
                    
                    [self->_videoListArr addObject:temp];
                    NSLog(@"_videoListArr ARRAYY%@",self->_videoListArr);
                }
            }

            NSLog(@"count: %lu",(unsigned long)self.videoListArr.count) ;
            [self.tableview reloadData];
            [MBProgressHUD hideHUDForView:self.tableview animated:NO];
        }
        });
    }];
    [dataTask resume];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSUInteger rowCount;
    if (isFiltered)
        rowCount=_filteredArray.count;
    else
        rowCount=_videoListArr.count;
    return  rowCount;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    VideoTableViewCell *cell = [_tableview dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    if (isFiltered) {
        NSMutableDictionary *ktemp=[_filteredArray objectAtIndex:indexPath.row];
        
        cell.videoLbl.text=[[ktemp objectForKey:@"bhajanTitle"]description];
        cell.thumbnailPath.text=[[ktemp objectForKey:@"thumbnailsPath"]description];
        cell.videoPath.text=[[ktemp objectForKey:@"videoPath"]description];
        cell.videoSize.text=[[ktemp objectForKey:@"videoSize"]description];
        cell.tags.text=[[ktemp objectForKey:@"tags"]description];
        cell.filename.text=[[ktemp objectForKey:@"fileName"]description];
        cell.fileId.text=[[ktemp objectForKey:@"fileId"]description];
        
        NSString *urlstr = [imgUrl stringByAppendingString:[[ktemp objectForKey:@"thumbnailsPath"]description]];
        NSLog(@"URL PATH=%@",urlstr);
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            UIImage *img1 = [UIImage imageWithData:[NSData dataWithContentsOfURL:     [NSURL URLWithString:urlstr]]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                cell.imgVideo.image = img1;
                
            });
        });
    
        return cell;
    }
    else{
    NSMutableDictionary *ktemp=[_videoListArr objectAtIndex:indexPath.row];
    
    cell.videoLbl.text=[[ktemp objectForKey:@"bhajanTitle"]description];
    cell.thumbnailPath.text=[[ktemp objectForKey:@"thumbnailsPath"]description];
    cell.videoPath.text=[[ktemp objectForKey:@"videoPath"]description];
    cell.videoSize.text=[[ktemp objectForKey:@"videoSize"]description];
    cell.tags.text=[[ktemp objectForKey:@"tags"]description];
    cell.filename.text=[[ktemp objectForKey:@"fileName"]description];
    cell.fileId.text=[[ktemp objectForKey:@"fileId"]description];
        
    NSLog(@"videoPath PATH=%@  fileID =%@ videoPath2 PATH=%@ ",cell.videoPath.text,cell.fileId.text,[[ktemp objectForKey:@"videoPath"]description]);
        
    videoPathStr=[[ktemp objectForKey:@"videoPath"]description];
        
    cell.videoBtn.tag=indexPath.row;
    [cell.videoBtn addTarget:self action:@selector(videoDownloadBtn:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
    NSString *urlstr = [imgUrl stringByAppendingString:[[ktemp objectForKey:@"thumbnailsPath"]description]];
    NSLog(@"URL PATH=%@",urlstr);
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImage *img1 = [UIImage imageWithData:[NSData dataWithContentsOfURL:     [NSURL URLWithString:urlstr]]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            cell.imgVideo.image = img1;
            
        });
    });
        if (indexPath.row == _videoListArr.count-1) {
            _currentPage = _currentPage + 1 ;
            [self dataVideoParsing] ;
        }
        return cell;
    }
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    VideoTableViewCell *cell=[_tableview cellForRowAtIndexPath:indexPath];
    
    _videolbl=cell.videoLbl.text;
    
    _videoPath=videoPathStr;

   
    _indxp=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
    
    NSLog(@"indexpath==%ld videopath=%@ videolabel=%@",(long)indexPath.row,_videoPath,_videolbl);
    
    [self performSegueWithIdentifier:@"videoPlay"
                              sender:[self.tableview cellForRowAtIndexPath:indexPath]];
    
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([[segue identifier] isEqualToString:@"videoPlay"])
    {
        
        VideoPlayViewController *kvc = [segue destinationViewController];
        
        kvc.videolabel=_videolbl;
        kvc.videoPath=_videoPath;
        kvc.indxp=_indxp;
      
        NSLog(@"indexpath in prepare for segue==%@ & audio image=%@",_videolbl,_videoPath);
    }
}


#pragma mark - SFSafariViewControllerDelegate

- (void)safariViewControllerDidFinish:(SFSafariViewController *)controller {
    [controller dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Private methods

- (void)showRetryAlertWithError:(NSError *)error {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"tableView.errorAlert.title", @"") message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"tableView.errorAlert.dismiss", @"") style:UIAlertActionStyleCancel handler:^(__unused UIAlertAction *action) {
        
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"tableView.errorAlert.retry", @"") style:UIAlertActionStyleDefault handler:^(__unused UIAlertAction *action) {
        [self fetchData:nil];
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)handleResponse:(NSData *)data error:(NSError *)error {
    if(error) {
        //  [self showRetryAlertWithError:error];
        return;
    }
    
    NSError *JSONError;
    NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&JSONError];
    
    if(JSONError) {
        // [self showRetryAlertWithError:JSONError];
        return;
    }
    // parse data into models
    NSArray *results = responseDict[kJSONResultsKey];
    //    //    NSMutableDictionary *ktemp=[_pdfListArr objectAtIndex:indexPath.row];
    
    NSMutableDictionary *ktemp=[_videoListArr objectAtIndex:indexBtn];
    
    NSString *bhajanTitle=[[ktemp objectForKey:@"bhajanTitle"]description];
    
    //  NSArray *newModels = [StoryModel modelsFromArray:results];
    
    // create new index paths
    NSIndexSet *newIndexes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(_videoListArr.count, bhajanTitle)];
    NSMutableArray *newIndexPaths = [[NSMutableArray alloc] init];
    
    [newIndexes enumerateIndexesUsingBlock:^(NSUInteger idx, __unused BOOL *stop) {
        [newIndexPaths addObject:[NSIndexPath indexPathForRow:idx inSection:0]];
    }];
    
    // update data source
    self.numPages = [responseDict[kJSONNumPagesKey] integerValue];
    self.currentPage += 1;
    _videoListArr= [self.videoListArr arrayByAddingObject:bhajanTitle];
    
    // update table view
    [self.tableview beginUpdates];
    [self.tableview insertRowsAtIndexPaths:newIndexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableview endUpdates];
}

- (void)fetchData:(void(^)(void))completion {
    NSInteger hits = CGRectGetHeight(self.tableview.bounds) / 44.0;
    NSString *URLString = [NSString stringWithFormat:kAPIEndpointURL, @(hits), @(self.currentPage)];
    NSURL *requestURL = [NSURL URLWithString:URLString];
    
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithURL:requestURL completionHandler:^(NSData *data, __unused NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self handleResponse:data error:error];
            
            [[UIApplication sharedApplication] stopNetworkActivity];
            
            if(completion) {
                completion();
            }
        });
    }];
    
    [[UIApplication sharedApplication] startNetworkActivity];
    
    
    
    // I run -[task resume] with delay because my network is too fast
    NSTimeInterval delay = (_videoListArr.count == 0 ? 0 : 5);
    
    [task performSelector:@selector(resume) withObject:nil afterDelay:delay];
}



-(void)videoDownloadBtn:(UIButton*)sender{
    
    NSString *emailid=[[NSUserDefaults standardUserDefaults]stringForKey:@"username"];
    NSLog(@"email id==%@",emailid);
    
    UIButton *btn =(UIButton *)sender;
    NSLog(@"Btn Click...........%ld",(long)btn.tag);
    
    indexBtn=sender.tag;
    NSLog(@"INDEX video===%ld",indexBtn);
    if (emailid==NULL) {
        UIAlertController * alert=[UIAlertController
                                   
                                   alertControllerWithTitle:@"Sorry!" message:@"You can't download media because you are not login.For downloading you need to login first!!"preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        NSLog(@"you pressed ok, please button");
                                        
                                    }];
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else {
        [self saveVideoInDocumentsDirectory];
       //[self showAlert:@"Alert" :@"File downloaded!"];
       
    }
    
}
-(NSData *)saveVideoInDocumentsDirectory
{


    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
         NSMutableDictionary *ktemp=[_videoListArr objectAtIndex:indexBtn];
        NSString *fileid=[[ktemp objectForKey:@"fileId"]description];
        NSString *filename=[[ktemp objectForKey:@"fileName"]description];
        
        NSString *file=[NSString stringWithFormat:@"%@",[videoDownloadUrl stringByAppendingString:fileid]];
        
        
        NSLog(@"Downloading Started");
        NSString *urlToDownload =file;
        NSURL  *url = [NSURL URLWithString:urlToDownload];
        NSData *urlData = [NSData dataWithContentsOfURL:url];
        if ( urlData )
        {
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = [paths objectAtIndex:0];
            
          //  NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,@"thefile.mp4"];
            NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,filename];
            //saving is done on main thread
            dispatch_async(dispatch_get_main_queue(), ^{
                [urlData writeToFile:filePath atomically:YES];
                NSLog(@"File Saved !");
                [self showAlert:@"Alert" :@"File downloaded!"];
                printf("Video file == %s",[filePath UTF8String]);//2nd implemented
                UISaveVideoAtPathToSavedPhotosAlbum (filePath,self, @selector(video:didFinishSavingWithError: contextInfo:), nil);
            });
        }
        
    });
    
    return data;
}
- (void) video: (NSString *) videoPath didFinishSavingWithError: (NSError *) error contextInfo: (void *) contextInfo
{
    NSLog(@"Finished saving video with error: %@", error);
    
}
-(void)fetchAudioInDocumentsDirectory:(NSString*)filepath
{
    data = [NSData dataWithContentsOfFile:filePath]; // fetch image data from filepath
    NSLog(@"dataWithContentsOfFile:%@",data);
    
}
- (IBAction)viewDownloadBtnClciked:(id)sender {
    
    NSLog(@"fetchImageInDocumentsDirectory:");
    [self fetchAudioInDocumentsDirectory:filePath];
    
}
-(void)showAlert:(NSString *)title :(NSString*)message{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    int screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    switch (screenHeight) {
            
        case 1366:
        {
            return 170.0f;
        }
        case 1024:
        {
            return 170.0f;
        }
        default:
        {
            return 77.0f;
            break;
        }
    }
    return 77;
}

@end
