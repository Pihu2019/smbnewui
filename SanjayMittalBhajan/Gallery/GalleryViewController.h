
#import <UIKit/UIKit.h>

@interface GalleryViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UICollectionView *myCollectionView;

@property (nonatomic,strong) NSMutableArray *galleryArr,*thumbnailArr;
@property(nonatomic,retain)NSString *indxp,*galleryIdStr,*titleStr,*status,*message,*currentPageNo,*totalCount,*totalPageSize;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView2;
@property (strong, nonatomic) IBOutlet UIView *viewfull;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@end

                      
