
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LyricsOfflineDetailVC : UIViewController
@property(nonatomic,retain)NSString *lyricsTitleStr,*lyricsInfoStr,*lyricsType,*indxpath;

@property (weak, nonatomic) IBOutlet UILabel *lyricsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lyricsInfo;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;


@end

NS_ASSUME_NONNULL_END
