
#import "LyricsOfflineTableViewCell.h"

@implementation LyricsOfflineTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.pdfImg.layer.cornerRadius = self.pdfImg.frame.size.width / 2;
    self.pdfImg.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
