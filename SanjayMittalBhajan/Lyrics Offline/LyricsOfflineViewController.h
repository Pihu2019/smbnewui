
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LyricsOfflineViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableview;

@property (nonatomic, strong) NSMutableArray *arrLyricsFiles;
 @property(nonatomic,retain)NSString *indexp,*lyricsTitle,*lyricsPath;
@end

NS_ASSUME_NONNULL_END
