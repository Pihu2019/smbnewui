

#import <UIKit/UIKit.h>

@interface PdfTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *pdfImg;
@property (weak, nonatomic) IBOutlet UILabel *pdfPath;
@property (weak, nonatomic) IBOutlet UILabel *thumbnailPath;
@property (weak, nonatomic) IBOutlet UILabel *bhajantitle;
@property (weak, nonatomic) IBOutlet UILabel *pdfSize;
@property (weak, nonatomic) IBOutlet UILabel *downloadUrl;
@property (weak, nonatomic) IBOutlet UILabel *tags;
@property (weak, nonatomic) IBOutlet UILabel *lyricsInfo;
@property (weak, nonatomic) IBOutlet UILabel *lyricsType;
@property (weak, nonatomic) IBOutlet UIButton *pdfButton;
@property (weak, nonatomic) IBOutlet UILabel *fileName;
@property (weak, nonatomic) IBOutlet UILabel *scaleNumber;


@end
