

#import "LyricsDetailViewController.h"
#import "UILabel+PinchZoom.h"
@interface LyricsDetailViewController ()

@end

@implementation LyricsDetailViewController

- (void)viewDidLoad {
    int xPos = 0;
    [super viewDidLoad];
    
//    self.lyricsInfo.text=self.lyricsInfoStr;
    NSLog(@"lyrics info%@",self.lyricsInfo.text);
    self.scaleNumber.text=self.scaleNumberStr;

   
    int screenHeight = [UIScreen mainScreen].bounds.size.height;
    NSLog(@"screenheight==== %d",screenHeight);
    switch (screenHeight) {
        case 1366:
            self.lyricsInfo.text=self.lyricsInfoStr;
            break;
            
        case 1024:
            self.lyricsInfo.text=self.lyricsInfoStr;
            break;
            
            
        default:
            
            self.lyricsInfo.text=self.lyricsInfoStr;
                CGSize size = [self getLabelSizeforGCtypes:self.lyricsInfo];
                self.lyricsInfo.frame = CGRectMake(0, 0, size.width, xPos);
                xPos += size.height;
                self.scrollview.contentSize = CGSizeMake(0, xPos);
            

            break;
    
    }
        self.lyricsInfo.zoomEnabled = YES;
        self.lyricsInfo.minFontSize = 10;
        self.lyricsInfo.maxFontSize = 40;
        self.lyricsInfo.adjustsFontSizeToFitWidth = YES;
        [self.lyricsInfo setNeedsLayout];
    
    
}

- (CGSize)getLabelSizeforGCtypes:(UILabel*)label
{
    CGSize size = [label.attributedText boundingRectWithSize:CGSizeMake(label.frame.size.width, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil].size;
    size.height+=20;
    return size;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self.lyricsInfo setNeedsLayout];
}

@end
