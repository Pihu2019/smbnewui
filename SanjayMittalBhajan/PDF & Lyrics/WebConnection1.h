//
//  WebConnection1.h
//  SanjayMittalBhajan
//
//  Created by Punit on 15/11/18.
//  Copyright © 2018 Eshiksa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"

NS_ASSUME_NONNULL_BEGIN
@protocol WebRequestResult1
-(void) webResponse:(NSDictionary*)responseDictionary;
@end

@interface WebConnection1 : NSObject
{
    
    __weak id<WebRequestResult1> delegate;
    
    NSMutableData *webData;
    NSString *xmlData;
    AppDelegate *appDelegate;
}

@property(weak,nonatomic)    id<WebRequestResult1> delegate;
@property(strong,nonatomic) NSDictionary *responseDictionary;
-(void) makeConnection :(NSMutableURLRequest*)req;

@end

NS_ASSUME_NONNULL_END
