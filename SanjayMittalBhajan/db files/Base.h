//
//  Base.h
//  SanjayMittalBhajan
//
//  Created by Punit on 31/07/18.
//  Copyright © 2018 Eshiksa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Base : NSObject
extern NSString * const mainUrl;
extern NSString * const imgUrl;
extern NSString * const splashScreenUrl;
extern NSString * const videoDownloadUrl;
extern NSString * const videoUrl;


extern NSString * const login;
extern NSString * const registration;
extern NSString * const feedback;
extern NSString * const gallery;
extern NSString * const galleryfolder;
extern NSString * const events;
extern NSString * const audio;
extern NSString * const getGalleryFolders;
extern NSString * const galleryImage;
extern NSString * const pdfList;
extern NSString * const calender;
extern NSString * const upcomingEvent;
extern NSString * const videoList;
extern NSString * const audioList;
extern NSString * const splashScreen;


@end
