

#import "ProfileViewController.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_profileView.bounds
                                                   byRoundingCorners:(UIRectCornerBottomRight | UIRectCornerBottomLeft)
                                                         cornerRadii:CGSizeMake(8.0, 8.0)];
    
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = _profileView.bounds;
    maskLayer.path = maskPath.CGPath;
    _profileView.layer.mask = maskLayer;
    
    NSString *profileusername = [[NSUserDefaults standardUserDefaults]
                                 stringForKey:@"profileusername"];
    
    NSString *address = [[NSUserDefaults standardUserDefaults]
                         stringForKey:@"address"];
    
    NSString *email = [[NSUserDefaults standardUserDefaults]
                       stringForKey:@"email"];
    
    NSString *mobile = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"mobile"];
    
    self.username.text=profileusername;
    self.location.text=address;
    self.email.text=email;
    self.mobileNum.text=mobile;
    
}



@end
