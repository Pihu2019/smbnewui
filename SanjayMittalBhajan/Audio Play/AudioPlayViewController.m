
//http://35.200.153.165:8080/Sanjay_Mittal_Bhajans/apis/audio/53/1544434105.mp3


#import "AudioPlayViewController.h"
#import "Base.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AudioListTableViewCell.h"
#import "AudioInfo.h"
#import <QuartzCore/QuartzCore.h>
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "MyAudioPlayer.h"
#import "DGActivityIndicatorView.h"

@interface AudioPlayViewController ()
{
    NSString *finalUrl,*str;
    BOOL isSelected;
    NSURL *url;
    NSUInteger selectedPosition,previousPositionNew;
    AVAudioPlayer * mPlayer;
    MyAudioPlayer * myAudioPlayer;
    DGActivityIndicatorView *activityIndicatorView;
}
@end

@implementation AudioPlayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:NO];
        hud.mode = MBProgressHUDAnimationFade;
        hud.labelText = @"Buffering...";
    
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeLineScale tintColor:[UIColor redColor]];
    CGFloat width = self.view.bounds.size.width / 6.0f;
    CGFloat height = self.view.bounds.size.height / 6.0f;
    activityIndicatorView.frame = CGRectMake(20,40,width,height);
    
    myAudioPlayer = [MyAudioPlayer sharedInstance];
    mPlayer = myAudioPlayer.mAudioPlayer;
    
    [_audioListBtn setImage:[UIImage imageNamed:@"playlist36.png"] forState:UIControlStateNormal];
    isSelected=YES;
    _listView.hidden=YES;
    _waveView.hidden=NO;
    //_audioImgView.hidden=YES;
   // _indicator.hidden=YES;
    self.progressSlider.value=0.0;
    //if(_audioListArr == nil) NSLog(@"passed arrayis null   %@",_audioListArr);
    [self.tableview setSeparatorColor:[UIColor clearColor]];
    
    NSLog(@"passed index null %tu ",self.selectedIndex);
    
    _tableview.delegate=self;
    _tableview.dataSource=self;
    _imgarray=[[NSMutableArray alloc]init];
    
    CFTimeInterval startTime = CACurrentMediaTime();
    
    dispatch_async( dispatch_get_main_queue(),
                   ^{
                    
                       [self playSongFromDataAt:self.selectedIndex];
                    
                       NSError *categoryError = nil;
                       [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&categoryError];
                       if (categoryError) {
                           NSLog(@"Error setting category! %@", [categoryError description]);
                       }
                        NSError *activationError = nil;
                       BOOL success=[[AVAudioSession sharedInstance] setActive:YES error:&activationError];
                       if (!success) {
                           if (activationError) {
                               NSLog(@"Could not activate audio session. %@", [activationError localizedDescription]);
                           } else {
                               NSLog(@"audio session could not be activated!");
                           }
                       }
                       [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
                       [super viewDidLoad];


                   });

    CFTimeInterval elapsedTime = CACurrentMediaTime() - startTime;
    NSLog(@"****playSongFromDataAt elapsedTime:%f",elapsedTime);//142.292120
}


-(void)updatePlayerUI:(AudioInfo *)mAudioInfo{
    
    self.kimgview.layer.cornerRadius = self.kimgview.frame.size.width / 2;
    self.kimgview.clipsToBounds = YES;
    self.fileName.text=mAudioInfo.fileName;
    self.fileId.text=mAudioInfo.fileId;
    self.bhajanTitle.text=mAudioInfo.bhajanTitleStr;
    self.audioPath.text=mAudioInfo.audioPathStr;
    
}
-(void)playSongFromDataAt:(int) index{
    AudioInfo *mAudio = [_audioListArr objectAtIndex:(NSUInteger)index];
    [self updatePlayerUI:mAudio];
    dispatch_async( dispatch_get_main_queue(),
                   ^{
                       if([self->mPlayer isPlaying]){
                           [self->mPlayer stop];
                           self.progressSlider.value=0.0;
                           [activityIndicatorView stopAnimating];
                           //mPlayer = nil;
                       }
                       [self playSongWithURL:[self audioMakeURL:mAudio.audioPathStr]];
                       
                       self.progressSlider.maximumValue=self->mPlayer.duration;
                       
                       self.sliderTimer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateSlider) userInfo:nil repeats:YES];
                       
                       [self.progressSlider addTarget:self action:@selector(progressSliderChanged:) forControlEvents:UIControlEventValueChanged];
                   });
}
-(void)playSongWithURL:(NSURL *)audioUrl{
    
    NSData *audioData = [NSData dataWithContentsOfURL:audioUrl];
    NSUInteger len = [audioData length];
    Byte *byteData = (Byte*)malloc(len);
    memcpy(byteData, [audioData bytes], len);
   
    
    NSError*error;
    //mPlayer = nil;

   if (mPlayer==nil || [mPlayer isPlaying]) {
    NSLog(@"audio player name.....%@",mPlayer);
        NSLog(@"audio player.....");
       error = [myAudioPlayer playSongWithData:audioData];
       mPlayer = myAudioPlayer.mAudioPlayer;
       mPlayer.delegate=self;
   }else{
       error = [myAudioPlayer playSongWithData:audioData];
       mPlayer = myAudioPlayer.mAudioPlayer;
       mPlayer.delegate=self;
       
   }
    
    if (error) {
        NSLog(@"FAILURE:%@",error.localizedDescription);
               }
    else{
        self.progressSlider.value=0.0;
        self.durationTimeLabel.text=[self stringFromInterval:mPlayer.duration];
        
        if (mPlayer.duration <= 3600) {
            self.currentTimeLabel.text=[NSString stringWithFormat:@"00:00"];
        }
        else{
            self.currentTimeLabel.text=[NSString stringWithFormat:@"0:00:00"];
        }
        [self.currentTimeLabel sizeToFit];
        
        [mPlayer prepareToPlay];
        NSLog(@"****Player start*****");
        [mPlayer play];
        //_audioImgView.hidden=NO;
        
      
     //   activityIndicatorView.frame = CGRectMake(0.0f, 0.0f, 50.0f, 50.0f);
        [self.view addSubview:activityIndicatorView];
        [activityIndicatorView startAnimating];
        
        [MBProgressHUD hideHUDForView:self.view animated:NO];
    }
    
}

-(NSURL*)audioMakeURL:(NSString *)audioPath{
    
    NSTimeInterval d= [[NSDate date] timeIntervalSince1970];
    NSString *currentTime=[NSString stringWithFormat:@"%f",d];
    
    NSArray *items=[currentTime componentsSeparatedByString:@"."];
    NSString *stritems=[items objectAtIndex:0];
    
    NSString *mp3=[stritems stringByAppendingFormat:@".mp3"];
    NSLog(@"***mp3***%@",mp3);
    
    NSString *str3=[NSString stringWithFormat:@"%@",[imgUrl stringByAppendingString:audioPath]];
    
    NSString *str1=[@"/" stringByAppendingFormat:@"%@",mp3];
    NSLog(@"***str1***%@",str1);
    
    NSString *str=[str3 stringByAppendingFormat:@"%@",str1];
    NSLog(@"***strNew***%@",str);
    
    
    NSURL *finalURL = [NSURL URLWithString:[str stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
    
    
    url=finalURL;
    NSLog(@"url***%@",url);
    
    return finalURL;
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    int i=[_indxpath intValue];
    NSString *tempimgstr=[_kimgarray objectAtIndex:i];
    
    [_kimgview sd_setImageWithURL:[NSURL URLWithString:tempimgstr]placeholderImage:[UIImage imageNamed:@"Audioimage.png"]];
    
   
        [super viewDidAppear:(BOOL)animated];
        [self.navigationController setNavigationBarHidden:NO animated:NO];
        


    
    
}
-(NSString *)stringFromInterval:(NSTimeInterval)interval{
    
    NSInteger ti=(NSInteger)interval;
    int seconds=ti % 60;
    int minutes=(ti/60)%60;
    long int hourss=(ti / 3600);
    if (ti <= 3600) {
        return [NSString stringWithFormat:@"%02d:%02d",minutes,seconds];
    }
    return [NSString stringWithFormat:@"%ld:%02d:%02d",hourss,minutes,seconds];
    
    return 0;
}
- (IBAction)playPauseAudio:(id)sender {
    [self playPauseMethod];
    
}
-(void)playPauseMethod{
    if (!mPlayer.playing) {
        self.progressSlider.maximumValue=mPlayer.duration;
        
        self.sliderTimer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateSlider) userInfo:nil repeats:YES];
        
        [self.progressSlider addTarget:self action:@selector(progressSliderChanged:) forControlEvents:UIControlEventValueChanged];
        
        [mPlayer play];
        [activityIndicatorView startAnimating];
        [self.playPauseBtn setImage:[UIImage imageNamed:@"pausebutton36.png"] forState:UIControlStateNormal];
        
    }else if (mPlayer.playing){
        [mPlayer pause];
        [activityIndicatorView stopAnimating];
        [self.playPauseBtn setImage:[UIImage imageNamed:@"playbutton36.png"] forState:UIControlStateNormal];
    }
    
}
-(void)updateSlider{
    self.progressSlider.value=mPlayer.currentTime;
    self.currentTimeLabel.text=[self stringFromInterval:mPlayer.currentTime];
    
}

- (IBAction)audioListBtnClicked:(UIButton *)sender {
    
    if (isSelected==YES) {
        
        [_audioListBtn setImage:[UIImage imageNamed:@"downArrow36.png"] forState:UIControlStateNormal];
        _listView.hidden=NO;
        _tableview.hidden=NO;
        isSelected=NO;
        _waveView.hidden=YES;
       
        
    }
    else{
        [_audioListBtn setImage:[UIImage imageNamed:@"playlist36.png"] forState:UIControlStateNormal];
        isSelected=YES;
        _listView.hidden=YES;
        _tableview.hidden=YES;
        _waveView.hidden=NO;
    }
    
    
}
//- (IBAction)stopAudio:(id)sender {
//    [self stopAudioMtd];
//}
-(void)playSongMtd{
    if (mPlayer.isPlaying) {
        self.progressSlider.maximumValue=mPlayer.duration;
        
        self.sliderTimer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateSlider) userInfo:nil repeats:YES];
        
        [self.progressSlider addTarget:self action:@selector(progressSliderChanged:) forControlEvents:UIControlEventValueChanged];
        [mPlayer play];
    }
    
}
-(void)stopAudioMtd{
    if (mPlayer.isPlaying) {
        [mPlayer stop];
    }
    [mPlayer setCurrentTime:0.0];
    [self.sliderTimer invalidate];
    self.progressSlider.value=0.0;

    if (mPlayer.duration <= 3600) {
        self.currentTimeLabel.text=[NSString stringWithFormat:@"00:00"];
    }
    else{
        self.currentTimeLabel.text=[NSString stringWithFormat:@"0:00:00"];
    }
    [self.currentTimeLabel sizeToFit];
    [self.playPauseBtn setImage:[UIImage imageNamed:@"playbutton36.png"] forState:UIControlStateNormal];


}
- (IBAction)adjustVolume:(id)sender {
    if (mPlayer!=nil) {
        mPlayer.volume=self.volumeSlider.value;
    }
}

- (IBAction)progressSliderChanged:(id)sender {
    [mPlayer stop];
    [mPlayer setCurrentTime:self.progressSlider.value];
    [mPlayer prepareToPlay];
    [mPlayer play];
}
-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    if (flag) {
      //  [self stopAudio:nil];
    }
}
-(void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error{
    NSLog(@"dECODER FAILED:%@",error.localizedDescription);
}
-(void)audioPlayerBeginInterruption:(AVAudioPlayer *)player{
    //audio play begin
}
-(void)audioPlayerEndInterruption:(AVAudioPlayer *)player withOptions:(NSUInteger)flags{
    if (flags==AVAudioSessionInterruptionOptionShouldResume && mPlayer!=nil) {
        [mPlayer play];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _audioListArr.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AudioListTableViewCell *cell=[_tableview dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    

    AudioInfo *ktemp=[_audioListArr objectAtIndex:indexPath.row];
    
    cell.audioTitle.text=ktemp.bhajanTitleStr;
    cell.thumbnailPath.text=ktemp.thumbnailPathStr;
    cell.audioPath.text=ktemp.audioPathStr;
    cell.audioSize.text=ktemp.audioSize;
    cell.tags.text=ktemp.tag;
    cell.fileId.text= ktemp.fileId;
    cell.fileName.text=ktemp.fileName;
    

    NSString *urlstr = [imgUrl stringByAppendingString:ktemp.thumbnailPathStr];
    
    NSLog(@"URL PATH=%@",urlstr);
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImage *img1 = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlstr]]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            cell.audioImg.image = img1;
            
        });
    });
    
    return cell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
//    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:NO];
//    hud.mode = MBProgressHUDModeAnnularDeterminate;
//    hud.labelText = @"Buffering...";
    // [self stopAudioMtd];
    _selectedIndex = indexPath.row;
    _previousIndex = _selectedIndex - 1;
    _nextIndex=_selectedIndex+1;
    
    [self playSongFromDataAt:_selectedIndex];
    
    //[MBProgressHUD hideHUDForView:self.view animated:NO];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    int screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    switch (screenHeight) {
            
        case 1366:
        {
            return 90.0f;
        }
        case 1024:
        {
            return 90.0f;
        }
        default:
        {
            return 50.0f;
            break;
        }
    }
    return 50;
    
}
- (IBAction)nextBtnClicked:(id)sender {
    NSLog(@"next button clicked");
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:NO];
    hud.mode = MBProgressHUDModeAnnularDeterminate;
    hud.labelText = @"Buffering...";
     CFTimeInterval startTime = CACurrentMediaTime();
    _nextIndex=_selectedIndex+1;
    if(_nextIndex < _audioListArr.count)
    {
      
        _previousIndex = _selectedIndex;
        _selectedIndex =_nextIndex;
        [self playSongFromDataAt:self.selectedIndex];
        _nextIndex++;
        
    }
      [MBProgressHUD hideHUDForView:self.view animated:NO];
    CFTimeInterval elapsedTime = CACurrentMediaTime() - startTime;
    NSLog(@"****next button clicked elapsedTime:%f",elapsedTime);//14.909196
}

- (IBAction)previousBtnClicked:(id)sender {
      NSLog(@"previous  button clicked");
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:NO];
    hud.mode = MBProgressHUDModeAnnularDeterminate;
    hud.labelText = @"Buffering...";
    
      CFTimeInterval startTime = CACurrentMediaTime();
    _previousIndex=_selectedIndex-1;
    if(_previousIndex >= 0)
    {
        _nextIndex = _selectedIndex;
        _selectedIndex= _previousIndex;
        [self playSongFromDataAt:self.selectedIndex];
        _previousIndex--;
    }
       [MBProgressHUD hideHUDForView:self.view animated:NO];
    CFTimeInterval elapsedTime = CACurrentMediaTime() - startTime;
    NSLog(@"****previous button clicked elapsedTime:%f",elapsedTime);//11.423854
}

/*+(void)initSession
{
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(remoteControlReceivedWithEvent:)
                                                 name: AVAudioSessionInterruptionNotification
                                               object: [AVAudioSession sharedInstance]];
    
    
    //set audio category with options - for this demo we'll do playback only
    NSError *categoryError = nil;
    [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayback error:&categoryError];
    
    if (categoryError) {
        NSLog(@"Error setting category! %@", [categoryError description]);
    }
    
    //activation of audio session
    NSError *activationError = nil;
    BOOL success = [[AVAudioSession sharedInstance] setActive: YES error: &activationError];
    if (!success) {
        if (activationError) {
            NSLog(@"Could not activate audio session. %@", [activationError localizedDescription]);
        } else {
            NSLog(@"audio session could not be activated!");
        }
    }
    
}
 */
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
}

-(void)viewWillDisappear:(BOOL)animated {
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    [self resignFirstResponder];
    [super viewWillDisappear:animated];
}

- (BOOL) canBecomeFirstResponder {
    return YES;
}

/*
- (void) remoteControlReceivedWithEvent: (UIEvent *) receivedEvent {
    NSLog(@"received event!");
    if (receivedEvent.type == UIEventTypeRemoteControl) {
        switch (receivedEvent.subtype) {
            case UIEventSubtypeRemoteControlTogglePlayPause: {
                if ([self mPlayer].rate > 0.0) {
                    [[self mPlayer] pause];
                } else {
                    [[self mPlayer] play];
                }
                
                break;
            }
            case UIEventSubtypeRemoteControlPlay: {
                [[self mPlayer] play];
                break;
            }
            case UIEventSubtypeRemoteControlPause: {
                [[self mPlayer] pause];
                break;
            }
            default:
                break;
        }
    }
}
*/
@end
/*-(void)playSongWithURL:(NSURL *)audioUrl{
 
 NSData *audioData = [NSData dataWithContentsOfURL:audioUrl];
 
 // if(mPlayer){
 if([mPlayer isPlaying]){
 [mPlayer stop];
 mPlayer = nil;
 }
 NSError*error;
 mPlayer=[[AVmPlayer alloc]initWithData:audioData error:&error];
 mPlayer.delegate=self;
 
 if (error) {
 NSLog(@"FAILURE:%@",error.localizedDescription);
 
 }
 else{
 /*NSMutableArray *images = [[NSMutableArray alloc]init];
 for (int imagenumber = 1; imagenumber <= 45; imagenumber++) {
 //NSMutableString *myString = [NSMutableString stringWithFormat:@"%i.png", imagenumber];
 [images addObject:[UIImage imageNamed:@"whitegray.png"]];
 }
 _audioImgView.animationImages = images;
 _audioImgView.animationDuration = 3.2;
 _audioImgView.animationRepeatCount = 1;
 [_audioImgView startAnimating];
 
 //  [self.view addSubview:imageview];
 
 self.progressSlider.value=0.0;
 self.durationTimeLabel.text=[self stringFromInterval:mPlayer.duration];
 
 if (mPlayer.duration <= 3600) {
 self.currentTimeLabel.text=[NSString stringWithFormat:@"00:00"];
 }
 else{
 self.currentTimeLabel.text=[NSString stringWithFormat:@"0:00:00"];
 }
 [self.currentTimeLabel sizeToFit];
 [mPlayer prepareToPlay];
 [mPlayer play];
 }
 }*/
////how to stroe data in cache???
//singleton class audio play method pass method

/*-(void)audioPlayBtnClicked{
 
 
 NSString *username = @"priya0102";
 NSString *password = @"priya12345";
 
 NSString *unpw = [NSString stringWithFormat:@"%@:%@",username,password];
 NSData *updata = [unpw dataUsingEncoding:NSASCIIStringEncoding];
 
 NSString *base64str = [NSString stringWithFormat:@"Basic %@", [updata base64Encoding]];
 NSDictionary *headers = @{ @"content-type": @"json/application",
 @"authorization": base64str };
 
 
 NSString *str3=[NSString stringWithFormat:@"%@",[imgUrl stringByAppendingString:_audioPathStr]];
 
 NSString *str1=[@"/" stringByAppendingFormat:@"%@",self.fileName.text];
 NSLog(@"***aa str1***%@",str1);
 
 NSString *str=[str3 stringByAppendingFormat:@"%@",str1];
 NSLog(@"***aa str***%@",str);
 
 
 NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:str] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
 [request setHTTPMethod:@"GET"];
 [request setAllHTTPHeaderFields:headers];
 
 NSURLSession *session = [NSURLSession sharedSession];
 NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
 if (error) {
 NSLog(@"%@", error);
 }
 else {
 
 NSLog(@"Success: %@", data);
 
 NSError *err;
 
 NSArray *jsonArray  = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
 NSLog(@"JSON DATA%@",jsonArray);
 
 NSDictionary *maindic=[NSJSONSerialization JSONObjectWithData:[NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil] options:NSJSONReadingMutableContainers error:nil];
 NSLog(@"response data:%@",maindic);
 
 }
 }];
 
 }
 */
