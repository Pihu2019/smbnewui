
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AudioInfo : NSObject

@property(nonatomic,retain)NSString *bhajanTitleStr,*audioPathStr,*thumbnailPathStr,*fileName,*audioSize,*pdfFileName,*fileId,*tag;

@end

NS_ASSUME_NONNULL_END
