

#import "AudioListTableViewCell.h"

@implementation AudioListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.audioImg.layer.cornerRadius = self.audioImg.frame.size.width / 2;
    self.audioImg.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
