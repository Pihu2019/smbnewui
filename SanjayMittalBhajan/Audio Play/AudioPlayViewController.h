

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>
#import <AudioToolbox/AudioToolbox.h>
#import "AudioInfo.h"
@interface AudioPlayViewController : UIViewController<AVAudioPlayerDelegate,UITableViewDelegate,UITableViewDataSource>
{
    dispatch_queue_t queue;
    AVPlayer *player;
    NSTimer *playbackTimer;
}
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property(nonatomic,retain)NSString *fileIdStr,*fileNameStr,*indxpath,*bhajanTitleStr;

@property (weak, nonatomic) IBOutlet UIButton *audioListBtn;
@property (weak, nonatomic) IBOutlet UIView *listView;
@property (weak, nonatomic) IBOutlet UILabel *fileName;
@property (weak, nonatomic) IBOutlet UILabel *fileId;
@property (weak, nonatomic) IBOutlet UILabel *bhajanTitle;
@property (weak, nonatomic) IBOutlet UIView *waveView;
@property (weak, nonatomic) IBOutlet UIButton *playPauseBtn;
- (IBAction)progressSliderChanged:(id)sender;
@property (weak, nonatomic) IBOutlet UISlider *progressSlider;
@property (weak, nonatomic) IBOutlet UILabel *currentTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *durationTimeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *kimgview;
@property (weak, nonatomic) IBOutlet UILabel *audioPath;

- (IBAction)nextBtnClicked:(id)sender;
- (IBAction)previousBtnClicked:(id)sender;

@property(strong,nonatomic)AVAudioPlayer *audioPlayer;
@property (weak, nonatomic) IBOutlet UIImageView *audioImgView;

@property(strong,nonatomic)NSTimer *sliderTimer;
-(NSString *)stringFromInterval:(NSTimeInterval)interval;
-(void)updateSlider;
-(void)playSongWithURL:(NSURL *)audioUrl;
@property (weak, nonatomic) IBOutlet UITableView *tableview;

@property(nonatomic,retain)UIImage *kimg;
@property(nonatomic,retain)NSMutableArray *kimgarray,*audioListArr,*imgarray;
@property (weak, nonatomic) IBOutlet UISlider *volumeSlider;
@property(nonatomic,retain)NSString *indxp,*status,*message,*details,*currentPageNo,*totalCount,*totalPageSize,*fileIdStr2,*filenameStr2,*bhajanTitle2,*audioPathStr,*audiopathStr2,*fileIdStr3;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;
@property (weak, nonatomic) IBOutlet UIButton *previousBtn;

@property(nonatomic,retain)AudioInfo *audioInfo;
@property(nonatomic,readwrite) int selectedIndex,nextIndex,previousIndex;


@end


//how to share class instance whole app??

