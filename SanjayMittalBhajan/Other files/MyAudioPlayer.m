
#import "MyAudioPlayer.h"

@interface MyAudioPlayer ()

@end

@implementation MyAudioPlayer
@synthesize mAudioPlayer;

+(MyAudioPlayer*)sharedInstance
{
    static MyAudioPlayer *sharedMyAudioPlayer;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyAudioPlayer  = [[MyAudioPlayer alloc] init];
    });
    return sharedMyAudioPlayer;
}

- (id)init
{
    if (self = [super init]) {
        mAudioPlayer = nil;
    }
    return self;
}

- (NSError*)playSongWithData:(NSData *)audioData
{
    NSError* error;
    mAudioPlayer = [[AVAudioPlayer alloc]initWithData:audioData error:&error];
    return error;
}

- (NSError*)playSongWithFileURL:(NSURL *)audioURL
{
    NSError* error;
    mAudioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:audioURL error:&error];
    return error;
}

@end
