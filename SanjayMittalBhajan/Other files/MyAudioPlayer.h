

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>
#import <AudioToolbox/AudioToolbox.h>

@interface MyAudioPlayer : NSObject

+ (MyAudioPlayer *)sharedInstance;

- (NSError*)playSongWithData:(NSData *)audioData;


- (NSError*)playSongWithFileURL:(NSURL *)audioURL;

@property(nonatomic,retain) AVAudioPlayer * mAudioPlayer;

@end
