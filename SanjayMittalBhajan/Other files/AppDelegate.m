
#import "AppDelegate.h"
#import "Reachability.h"
#import "LoginViewController.h"
#import "HomeViewController.h"
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    sleep(3);
    
    Reachability *reach = [Reachability reachabilityForInternetConnection];
    NetworkStatus netStatus = [reach currentReachabilityStatus];
    if (netStatus == NotReachable){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"No network connection" message:@"Please check your internet and try again." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alert show];
    }
    else {
        [[UINavigationBar appearance]setBarTintColor:[UIColor colorWithRed:195.0/255.0 green:49.0/255.0 blue:98.0/255.0 alpha:1.0]];
        NSShadow *shadow = [[NSShadow alloc] init];
        shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
        shadow.shadowOffset = CGSizeMake(0, 1);
        [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                               [UIColor whiteColor], NSForegroundColorAttributeName,shadow, NSShadowAttributeName,
                                                               [UIFont fontWithName:@"HelveticaNeue-CondensedBlack" size:21.0], NSFontAttributeName, nil]];
        
        [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
        
        UIStoryboard *storyboard = [self grabStoryboard];
        self.window.rootViewController = [storyboard instantiateInitialViewController];
        [self.window makeKeyAndVisible];
        
        NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"username"];
        NSLog(@"savedValue==%@",savedValue);

        if(savedValue==NULL)
        {
            int screenHeight = [UIScreen mainScreen].bounds.size.height;
            switch (screenHeight) {
          case 1366:
            {
                storyboard = [UIStoryboard storyboardWithName:@"Main2" bundle:nil];
                LoginViewController *loginController=[[UIStoryboard storyboardWithName:@"Main2" bundle:nil]instantiateViewControllerWithIdentifier:@"identifier"];
                UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
                self.window.rootViewController = navController;
                
                
                break;
            }
        case 1024:
            {
                storyboard = [UIStoryboard storyboardWithName:@"Main2" bundle:nil];
                LoginViewController *loginController=[[UIStoryboard storyboardWithName:@"Main2" bundle:nil]instantiateViewControllerWithIdentifier:@"identifier"];
                UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
                self.window.rootViewController = navController;
                
                
                break;
            }
            
        default:
            {

                    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    LoginViewController *loginController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"identifier"];
                    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
                    self.window.rootViewController = navController;
                
                break;
            }
          }
        }
        else
        {
            [self navigatingFromLogin];
        }
    }
    
    return YES;
    
}

-(void)navigatingFromLogin{
    UIStoryboard *storyboard = [self grabStoryboard];

    HomeViewController *admin = [storyboard instantiateViewControllerWithIdentifier:@"AfterLogin"];

    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:admin];
    self.window.rootViewController = navController;

}
- (UIStoryboard *)grabStoryboard {
    
    int screenHeight = [UIScreen mainScreen].bounds.size.height;
    NSLog(@"screenheight==== %d",screenHeight);
    UIStoryboard *storyboard;
    switch (screenHeight) {
        case 1366:
            storyboard = [UIStoryboard storyboardWithName:@"Main2" bundle:nil];
            break;
            
        case 1024:
            storyboard = [UIStoryboard storyboardWithName:@"Main2" bundle:nil];
            break;
            
            
        default:
            
            storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            break;
            
    }
    
    return storyboard;
}
- (void)applicationWillResignActive:(UIApplication *)application {
   
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
   
}


- (void)applicationWillTerminate:(UIApplication *)application {
   
}
-(void)showIndicator:(NSString *)withTitleString view1:(UIView *)currentView
{
    // The hud will dispable all input on the view
    actIndicator = [[MBProgressHUD alloc] initWithView:currentView];
    // Add HUD to screen
    [currentView addSubview:actIndicator];
    actIndicator.labelText = withTitleString;
    [actIndicator show:YES];
}

-(void)hideIndicator
{
    [actIndicator show:NO];
    [actIndicator removeFromSuperview];
     actIndicator = nil;
}

@end
