

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>
#import <AudioToolbox/AudioToolbox.h>
@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    MBProgressHUD *actIndicator;
  
}
@property (strong, nonatomic) UIWindow *window;

-(void)showIndicator:(NSString *)withTitleString view1:(UIView *)currentView;
-(void)hideIndicator;
@property (strong,nonatomic) UINavigationController *navigationController;
@end

