
#import "HomeViewController.h"
#import "Base.h"
#import <QuartzCore/QuartzCore.h>
@interface HomeViewController ()
@end
@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupScrollView:self.scroll];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                             forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
   [self.navigationController.navigationBar setTitleTextAttributes:@{
                                            NSForegroundColorAttributeName : [UIColor clearColor]
                                                                      }];
    

    self.movingLabel.tag = 601;
    self.movingLabel.marqueeType = MLContinuous;
    self.movingLabel.scrollDuration = 15.0f;
    self.movingLabel.fadeLength = 15.0f;
    self.movingLabel.trailingBuffer = 30.0f;
    
    UIPageControl *pgCtr=[[UIPageControl alloc]initWithFrame:CGRectMake(0, 264, 480, 36)];
    [pgCtr setTag:20];
    pgCtr.numberOfPages=10;
    pgCtr.autoresizingMask=UIViewAutoresizingNone;
    [self.contentview addSubview:pgCtr];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
   
    gradient.colors = @[(id)[UIColor colorWithRed:(225.0/225.0) green:(0.0/225.0) blue:(84.0/255.0)alpha:1.0].CGColor,(id)[UIColor colorWithRed:(244.0/225.0) green:(219.0/225.0) blue:(72.0/255.0)alpha:1.0].CGColor];
    
   
    CAGradientLayer *gradient1 = [CAGradientLayer layer];
    gradient1.frame = _audioBtn.bounds;
    gradient1.colors = @[(id)[UIColor colorWithRed:(247.0/225.0) green:(164.0/225.0) blue:(83.0/255.0)alpha:1.0].CGColor,(id)[UIColor colorWithRed:(246.0/225.0) green:(91.0/225.0) blue:(114.0/255.0)alpha:1.0].CGColor];
    
    [_audioBtn.layer insertSublayer:gradient1 atIndex:0];
    
    CAGradientLayer *gradient2 = [CAGradientLayer layer];
    gradient2.frame = _videoBtn.bounds;
    gradient2.colors = @[(id)[UIColor colorWithRed:(51.0/225.0) green:(141.0/225.0) blue:(242.0/255.0)alpha:1.0].CGColor,(id)[UIColor colorWithRed:(151.0/225.0) green:(89.0/225.0) blue:(247.0/255.0)alpha:1.0].CGColor];
    
    [_videoBtn.layer insertSublayer:gradient2 atIndex:0];
    
    CAGradientLayer *gradient3 = [CAGradientLayer layer];
    gradient3.frame = _galleryBtn.bounds;
    gradient3.colors = @[(id)[UIColor colorWithRed:(68.0/225.0) green:(217.0/225.0) blue:(66.0/255.0)alpha:1.0].CGColor,(id)[UIColor colorWithRed:(2.0/225.0) green:(215.0/225.0) blue:(177.0/255.0)alpha:1.0].CGColor];
    
    [_galleryBtn.layer insertSublayer:gradient3 atIndex:0];
    
    CAGradientLayer *gradient4 = [CAGradientLayer layer];
    gradient4.frame = _shareBtn.bounds;
    gradient4.colors = @[(id)[UIColor colorWithRed:(245.0/225.0) green:(214.0/225.0) blue:(2.0/255.0)alpha:1.0].CGColor,(id)[UIColor colorWithRed:(247.0/225.0) green:(158.0/225.0) blue:(0.0/255.0)alpha:1.0].CGColor];
    
    [_shareBtn.layer insertSublayer:gradient4 atIndex:0];
    
    
    CAGradientLayer *gradient5 = [CAGradientLayer layer];
    gradient5.frame = _feedbaclBtn.bounds;
    gradient5.colors = @[(id)[UIColor colorWithRed:(240.0/225.0) green:(102.0/225.0) blue:(232.0/255.0)alpha:1.0].CGColor,(id)[UIColor colorWithRed:(127.0/225.0) green:(120.0/225.0) blue:(245.0/255.0)alpha:1.0].CGColor];
    
    [_feedbaclBtn.layer insertSublayer:gradient5 atIndex:0];
    
    CAGradientLayer *gradient6 = [CAGradientLayer layer];
    gradient6.frame = _lyricsBtn.bounds;
    gradient6.colors = @[(id)[UIColor colorWithRed:(98.0/225.0) green:(198.0/225.0) blue:(240.0/255.0)alpha:1.0].CGColor,(id)[UIColor colorWithRed:(30.0/225.0) green:(129.0/225.0) blue:(183.0/255.0)alpha:1.0].CGColor];
    
    [_lyricsBtn.layer insertSublayer:gradient6 atIndex:0];
    
    UIBezierPath *maskPath1 = [UIBezierPath bezierPathWithRoundedRect:_homeView.bounds
                                                   byRoundingCorners:(UIRectCornerBottomRight | UIRectCornerBottomLeft)
                                                         cornerRadii:CGSizeMake(8.0, 8.0)];
    CAShapeLayer *maskLayer1 = [CAShapeLayer layer];
    maskLayer1.frame = _homeView.bounds;
    maskLayer1.path = maskPath1.CGPath;
    _homeView.layer.mask = maskLayer1;
    
}
-(void)setupScrollView:(UIScrollView *)scrMain{
    for (int i=1; i<=10; i++) {

        NSString *mainstr1=[NSString stringWithFormat:@"%@",[splashScreenUrl stringByAppendingString:@"1"]];
        NSString *mainstr2=[NSString stringWithFormat:@"%@",[splashScreenUrl stringByAppendingString:@"2"]];
        NSString *mainstr3=[NSString stringWithFormat:@"%@",[splashScreenUrl stringByAppendingString:@"3"]];
        NSString *mainstr4=[NSString stringWithFormat:@"%@",[splashScreenUrl stringByAppendingString:@"4"]];
        NSString *mainstr5=[NSString stringWithFormat:@"%@",[splashScreenUrl stringByAppendingString:@"5"]];
        NSString *mainstr7=[NSString stringWithFormat:@"%@",[splashScreenUrl stringByAppendingString:@"6"]];
        NSString *mainstr8=[NSString stringWithFormat:@"%@",[splashScreenUrl stringByAppendingString:@"7"]];
        NSString *mainstr9=[NSString stringWithFormat:@"%@",[splashScreenUrl stringByAppendingString:@"8"]];
        NSString *mainstr10=[NSString stringWithFormat:@"%@",[splashScreenUrl stringByAppendingString:@"9"]];
        NSString *mainstr11=[NSString stringWithFormat:@"%@",[splashScreenUrl stringByAppendingString:@"10"]];

        NSLog(@"URLLLLLLL 2 %@",mainstr1);
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            UIImage *img1 = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:mainstr1]]];
             UIImage *img2 = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:mainstr2]]];
            UIImage *img3 = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:mainstr3]]];
            UIImage *img4 = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:mainstr4]]];
            UIImage *img5 = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:mainstr5]]];
            UIImage *img7 = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:mainstr7]]];
            UIImage *img8 = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:mainstr8]]];
            UIImage *img9 = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:mainstr9]]];
            UIImage *img10 = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:mainstr10]]];
            UIImage *img11 = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:mainstr11]]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIImage *image = img1;
                UIImage *image2 = img2;
                UIImage *image3 = img3;
                UIImage *image4 = img4;
                UIImage *image5 = img5;
                UIImage *image7 = img7;
                UIImage *image8 = img8;
                UIImage *image9 = img9;
                UIImage *image10 = img10;
                UIImage *image11 = img11;
                
                UIImageView *imgView=[[UIImageView alloc]initWithFrame:CGRectMake((i-1)*scrMain.frame.size.width, 0, scrMain.frame.size.width, scrMain.frame.size.height)];
                
                imgView.contentMode=UIViewContentModeScaleToFill;
                [imgView setImage:image];
                
              //  imgView.image=[UIImage imageNamed:@"newOne.jpg"]; //to set static one img
                
        imgView.animationImages=@[image,image2,image3,image4,image5,image7,image8,image9,image10,image11];
                imgView.animationDuration=8.0;
                imgView.tag=i+1;
                [scrMain addSubview:imgView];
                [imgView startAnimating];
            });
        });
    }
    [scrMain setContentSize:CGSizeMake(scrMain.frame.size.width*10, scrMain.frame.size.height)];
    
    [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(scrollingTimer) userInfo:nil repeats:YES];
    
}

-(void)scrollingTimer{
    UIScrollView *scrMain=(UIScrollView *)[self.view viewWithTag:1];
    UIPageControl *pgCtr=(UIPageControl*)[self.view viewWithTag:20];
    CGFloat contentOffset=self.scroll.contentOffset.x;
   
    int nextPage=(int)(contentOffset/self.scroll.frame.size.width)+1;
    
    if (nextPage!=10){
        [self.scroll scrollRectToVisible:CGRectMake(nextPage*self.scroll.frame.size.width,0,self.scroll.frame.size.width,self.scroll.frame.size.height)animated:YES];
        pgCtr.currentPage=nextPage;
    }else{
        [self.scroll scrollRectToVisible:CGRectMake(0,0,self.scroll.frame.size.width,self.scroll.frame.size.height)animated:YES];
        
        pgCtr.currentPage=0;
    }
}


-(void)splashMovingTextApi{
    
    //https://70de579a.ngrok.io/Sanjay_Mittal_Bhajans/apis/message/getList
    
 
}
- (IBAction)shareBtnClicked:(id)sender {
    
    NSString *textToShare = @"Sanjay Mittal Bhajans Mobile APP is a Sewa towards Shyam Baba with a mission to reach maximum Shyam Premis";
    NSURL *myWebsite = [NSURL URLWithString:@"https://play.google.com/store/apps/details?id=com.getEKart.sanjaymittalbhajans"];
    
    NSArray *objectsToShare = @[textToShare, myWebsite];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    //if iPhone
if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo,
                                   UIActivityTypePostToTwitter,
                                   UIActivityTypePostToFacebook,
                                   UIActivityTypePostToWeibo,
                                   UIActivityTypeMessage,
                                   UIActivityTypeMail,
                                   UIActivityTypeCopyToPasteboard,
                                   UIActivityTypePostToTencentWeibo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
    
    }
    //if iPad
    else {
        // Change Rect to position Popover
        UIPopoverController *popup = [[UIPopoverController alloc] initWithContentViewController:activityVC];
        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/4, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewDidLoad];
    
    [super viewDidAppear:(BOOL)animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
}

@end

