

#import <UIKit/UIKit.h>
#import "MarqueeLabel.h"
@interface HomeViewController : UIViewController<UIScrollViewDelegate>


@property (weak, nonatomic) IBOutlet UIButton *audioBtn;
@property (weak, nonatomic) IBOutlet UIButton *videoBtn;
@property (weak, nonatomic) IBOutlet UIButton *galleryBtn;
@property (weak, nonatomic) IBOutlet UIButton *shareBtn;
@property (weak, nonatomic) IBOutlet UIButton *feedbaclBtn;
@property (weak, nonatomic) IBOutlet UIButton *lyricsBtn;
@property (weak, nonatomic) IBOutlet UIButton *aboutBtn;
@property (weak, nonatomic) IBOutlet UIScrollView *scroll;
@property (weak, nonatomic) IBOutlet UIView *contentview;
@property (weak, nonatomic) IBOutlet UIView *homeView;
@property (nonatomic, weak) IBOutlet MarqueeLabel *movingLabel;

-(IBAction)shareBtnClicked:(id)sender;
@property(nonatomic,retain)NSString *indxp,*status,*message,*details;
@property (nonatomic,strong) NSMutableArray *splashArr;


@end
